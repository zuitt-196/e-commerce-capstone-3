const express = require("express");
const router = express.Router();

const auth = require("../auth")
const {verify, verifyAdmin} = auth;

const productControllers = require("../controllers/productControllers");

// ADD PRODUCT[SECTION]
router.post('/addProduct',verify,verifyAdmin,productControllers.addProduct);

router.get('/getAllActiveProduct',productControllers.getAllActiveProduct);

router.get('/getSingleProduct/:productIdproductId',productControllers.getSingleProduct)

router.put('/updateProduct/:productId',verify,verifyAdmin,productControllers.updateProduct);

router.delete('/archiveProduct/:productId',verify,verifyAdmin,productControllers.archiveProduct);

router.put('/activate/:productId',verify,verifyAdmin,productControllers.activateProduct);

module.exports = router;


