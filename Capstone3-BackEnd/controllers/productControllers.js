const User = require("../models/User");
const Product = require("../models/Product");

// Adding Product
module.exports.addProduct = (req,res) =>{
    let newProduct = new Product({
        name:req.body.name,
        description:req.body.description,
        price:req.body.price
    })
    newProduct.save()
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

// Getting Active Product
module.exports.getAllActiveProduct = (req,res) =>{
    Product.find({isActive:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}


module.exports.getSingleProduct = (req,res) =>{ 
       
     Product.findOne({id:req.params.productId})
    .then(result => {
        if(result.isActive){
            res.send(result)
            console.log(result)
        } else {
            res.send({message: "Not an Active Product."})
        }
    })
    .catch(error => res.send(error))

}

module.exports.updateProduct = (req,res) =>{
     let update = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.archiveProduct = (req,res) =>{
   let update = {
        isActive: false
    }

    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.activateProduct = (req,res) => {

    let update = {
        isActive: true
    }

    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

