const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');

const app = express();
const port = process.env.PORT || 4000;

// [DATABASE CONNECT]
mongoose.connect("mongodb+srv://admin:admin123@cluster0.uctt7.mongodb.net/Capstone3E-Commerce?retryWrites=true&w=majority",
{
    useNewUrlParser: true,
    useUnifiedTopology: true,

});


let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB Connection Error."));
db.once('open',()=>console.log("Connected to MongoDB."))

//[SECTION MIDDLE WARE ]
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use('/products',productRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use('/orders',orderRoutes);



app.listen(port,()=>console.log("Running is good po"))
































