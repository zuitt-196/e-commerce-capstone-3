import Carousel from 'react-bootstrap/Carousel';
import {Row,Col, Card, Button} from 'react-bootstrap';
export default function CarouselSec() {
    return (
      <>
   
     <div>
       <Row id = "row">
        <Col>
          
        <Carousel id = "carousel">
      <Carousel.Item interval={1000}>
      <img id = "imgCarousel" 
          className="d-block w-100 h-50"
          src="./images/carousel1.jpeg"
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={500}>
        <img  id = "imgCarousel" 
          className="d-block w-100 h-50"
          src="./images/carousel2.jpeg"
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img id = "imgCarousel" 
          className="d-block w-100 h-20"
          src="./images/carousel4.jpeg"
          alt="Third slide"
        />
      </Carousel.Item>
     </Carousel>
        </Col>
      
        <Col> 
        <Card style={{ width: '100%'}}>
      <Card.Img variant="top" src="./images/our_stores.jpeg" />
      <Card.Body>
        <Card.Title>OUR HISTORY</Card.Title>
        <Card.Text>
        For over a decade, Flight Club has changed the landscape of sneaker retail. Carrying every brand name on the market, Flight Club has evolved from a one-stop sneaker destination, to a cultural hub for sneaker enthusiasts and novices alike. From Air Jordans to Nike to Adidas and more, we have it all.
        </Card.Text>
        <Button variant="primary">Learn More</Button>
      </Card.Body>
    </Card>
    
        </Col>         
    </Row>
    </div>
 
        </>
    )
}

