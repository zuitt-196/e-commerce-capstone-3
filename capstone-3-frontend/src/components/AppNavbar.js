import { Link } from "react-router-dom";
import {useState, useContext} from "react";
import UserContext from "../UserContext"




import {Navbar, Nav, Container	} from "react-bootstrap";

export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return(



<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" id = "Nav">
      <Container>
            <Navbar.Brand as={Link} to="/" eventKey="/">
            <img id = "img"
              alt=""
             src="/images/logo.png"
              width="50"
              height="50"
			  border-radius="5px"
              className="d-inline-block align-top"/>
          </Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="ms-auto" defaultActiveKey="/">
	            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
	            {
									// if the user Admin log in will be proceed the Admin Dashboard page
									(user.isAdmin)
									?
										<Nav.Link as={Link} to="/admin" eventKey="/admin">Admin Dashboard</Nav.Link>
									:
										<>
											<Nav.Link as={Link} to="/products" eventKey="/products">Products</Nav.Link>
											<Nav.Link as={Link} to="/cart" eventKey="/cart">Cart</Nav.Link>
										</>
	            }

          </Nav>

          <Nav>
				
	            {	
									// if the user is logged  they are stay the regular page
									(user.id !== null)
									?
										<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
									:
										<>
											<Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
											<Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
										</>
	            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
