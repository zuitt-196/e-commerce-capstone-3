import {Row, Col, Card} from 'react-bootstrap';
// import CardGroup from 'react-bootstrap/CardGroup';


export default function Highlights(){
	return(

			<>
				<div id = "highlights">
					<h2 id ="text" >New Releases</h2>
	
				<br />
			<Row xs={1} md={4} className="g-4 pb-5">
	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="/images/product-1.webp" />
	            <Card.Body>
	              <Card.Title>High Quality</Card.Title>
	              <Card.Text>
	                This is a longer card with supporting text below as a natural
	                lead-in to additional content. This content is a little bit
	                longer.
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>

	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="./images/product-2.webp" />
	            <Card.Body>
	              <Card.Title>Fast Shipping</Card.Title>
	              <Card.Text>
	                This is a longer card with supporting text below as a natural
	                lead-in to additional content. This content is a little bit
	                longer.
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>

	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="./images/product-3.webp" />
	            <Card.Body>
	              <Card.Title>Affordable Prices</Card.Title>
	              <Card.Text> This is a longer card with supporting text below as a natural
	                lead-in to additional content. This content is a little bit
	                longer.
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>

	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="./images/product-4.webp" />
	            <Card.Body>
	              <Card.Title>Affordable Prices</Card.Title>
	              <Card.Text> This is a longer card with supporting text below as a natural
	                lead-in to additional content. This content is a little bit
	                longer.
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>
		
	    	</Row>	
		</div>
		</>
	  );
	}

