
import {useState, useEffect} from "react";
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import AppNavbar from "./components/AppNavbar";
import AppFooter from './components/AppFooter';
import AdminDashboard from "./pages/AdminDashboard";
import Cart from "./pages/Cart";
import Error from './pages/Error';
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Products from "./pages/Products";
import ViewProduct from "./pages/ViewProduct";
import Register from "./pages/Register";





import './App.css';
import { UserProvider } from "./UserContext";

function App() {


  const [user, setUser] = useState({

    id: null,
    isAdmin: null
  })


    const [product, setProduct] = useState({

      id: null,
      updatedProduct: []
    })


  const unsetUser = () =>{
    localStorage.clear();
  }


  useEffect(() =>{
    fetch('http://localhost:4000/users/getUserDetails',{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {


      if(typeof data._id !== "undefined"){
          setUser({

            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      else{
          setUser({
              id: null,
              isAdmin: null
            })
      }
      
    })
  }, [])

  return (

    
    <UserProvider value={{user, setUser, unsetUser, product}}>
        <Router>
          <AppNavbar />
          <Container>          
              <Routes> 
                  <Route exact path = "/" element={<Home />} />
                  <Route exact path = "/login" element={<Login />} />
                  <Route exact path = "/products" element={<Products />} />
                  <Route exact path = "/admin" element={<AdminDashboard />} />
                  <Route exact path = "/cart" element={<Cart />} />
                  <Route exact path = "/register" element={<Register />} />
                  <Route exact path = "/logout" element={<Logout />} />
                  <Route exact path = "/products/:productId" element={<ViewProduct />} />
                  <Route exact path="*" element={<Error/>}/> 
              </Routes>
          </Container>
          {/* <AppFooter/> */}
        </Router>
    </UserProvider>
  );
}


export default App;

